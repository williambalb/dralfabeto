import Chicken from './icons/chicken.svg';
import Navy from './icons/ship.svg';
import Tree from './icons/tree.svg';
import IceCream from './icons/ice-cream.svg';
import Apple from './icons/apple.svg';
import PiggyBank from './icons/piggy-bank.svg';
import Rabbit from './icons/rabbit.svg';
import Suitcase from './icons/suitcase.svg';
import Van from './icons/van.svg';
import Bird from './icons/bird.svg';
import EarthGlobe from './icons/earth-globe.svg';
import Igloo from './icons/igloo.svg';
import MexicanHat from './icons/mexican-hat.svg';

export default {
  Chicken,
  Navy,
  Tree,
  IceCream,
  Apple,
  PiggyBank,
  Rabbit,
  Suitcase,
  Van,
  Bird,
  EarthGlobe,
  Igloo,
  MexicanHat,
};
