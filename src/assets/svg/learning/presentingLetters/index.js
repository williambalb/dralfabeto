import Airplane from './icons/airplane.svg';
import Bee from './icons/bee.svg';
import Pineapple from './icons/pineapple.svg';
import ArrowR from './icons/right-arrow.svg';

export default {
  Airplane,
  Bee,
  Pineapple,
  ArrowR,
};
