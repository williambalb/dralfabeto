import Frog from './icons/frog.svg';
import Carrot from './icons/carrot.svg';
import King from './icons/king.svg';
import Heart from './icons/heart.svg';
import Jersey from './icons/jersey.svg';
import Mountain from './icons/mountain.svg';
import KinderGarten from './icons/kindergarten.svg';
import FeedingBottle from './icons/feeding-bottle.svg';
import Sunny from './icons/sunny.svg';
import Comb from './icons/comb.svg';
import Notebook from './icons/notebook.svg';
import Airplane from './icons/airplane.svg';

export default {
  Frog,
  Carrot,
  King,
  Heart,
  Jersey,
  Mountain,
  KinderGarten,
  FeedingBottle,
  Sunny,
  Comb,
  Notebook,
  Airplane,
};
