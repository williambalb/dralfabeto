import A from './icons/letter-a.svg';
import B from './icons/letter-b.svg';
import M from './icons/letter-m.svg';
import O from './icons/letter-o.svg';
import P from './icons/letter-p.svg';

export default {
  A,
  B,
  M,
  O,
  P,
};
