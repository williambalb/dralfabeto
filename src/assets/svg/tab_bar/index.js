import Letters from './icons/abc.svg';
import LettersLinear from './icons/abc_l.svg';
import Reading from './icons/child.svg';
import ReadingLinear from './icons/child_l.svg';
import Historys from './icons/book.svg';
import HistorysLinear from './icons/book_l.svg';
import Vocabulary from './icons/vocal.svg';
import VocabularyLinear from './icons/vocal_l.svg';
import Settings from './icons/config.svg';
import SettingsLinear from './icons/config_l.svg';

export default {
  Letters,
  LettersLinear,
  Reading,
  ReadingLinear,
  Historys,
  HistorysLinear,
  Vocabulary,
  VocabularyLinear,
  Settings,
  SettingsLinear,
};
