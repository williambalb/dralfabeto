import React from 'react';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {Bar, Button} from './styles';

const Actionbar = (props) => {
  const navigation = useNavigation();
  return (
    <Bar>
      {props.backHome ? (
        <Button onPress={() => navigation.navigate('Home')}>
          <Icon name="times" size={30} color="#fff" />
        </Button>
      ) : (
        <Button onPress={() => navigation.goBack()}>
          <Icon name="times" size={30} color="#fff" />
        </Button>
      )}
      {props.complete ? (
        <Button
          onPress={() =>
            navigation.navigate(props.destiny, {contentData: props.contentData})
          }>
          <Icon name="arrow-right" size={30} color="#fff" />
        </Button>
      ) : null}
    </Bar>
  );
};

export default Actionbar;
