import styled from 'styled-components/native';

export const Bar = styled.View`
  height: 35px;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  padding-left: 15px;
  padding-right: 15px;
`;

export const Button = styled.TouchableOpacity`
  width: 30px;
  height: 30px;
  align-items: center;
`;

export const Text = styled.Text`
  color: #fff;
  font-family: MontserratBold;
  font-size: 25px;
  text-align: center;
`;
