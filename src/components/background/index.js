import React from 'react';
import {Container} from './styles';

const background = ({children}) => {
  return <Container>{children}</Container>;
};

export default background;
