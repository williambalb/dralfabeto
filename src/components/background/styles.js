import styled from 'styled-components/native';

export const Container = styled.View`
  background-color: #abd6ea;
  flex: 1;
  justify-content: center;
  align-items: center;
`;
