import React from 'react';
import {Dimensions} from 'react-native';
import {SoundContainer, SoundButton} from './styles';
import SoundSvg from './../../assets/svg/sound.svg';
import SoundPlayer from 'react-native-sound-player';

const ButtonSound = (props) => {
  const windowWidth = Dimensions.get('window').width;

  const bucketUrl = 'https://dralfabetopolly.s3-sa-east-1.amazonaws.com/';

  function playSound() {
    try {
      SoundPlayer.loadUrl(
        bucketUrl + props.soundType + '.' + props.soundId + '.mp3',
      );
      SoundPlayer.addEventListener('FinishedLoadingURL', ({success}) => {
        SoundPlayer.play();
      });
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <SoundContainer style={{width: windowWidth}}>
      <SoundButton onPress={() => playSound()}>
        <SoundSvg width={50} height={50} />
      </SoundButton>
    </SoundContainer>
  );
};

export default ButtonSound;
