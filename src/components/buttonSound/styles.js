import styled from 'styled-components/native';

export const SoundContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 10px;
`;

export const SoundButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  width: 100%;
  height: 60px;
  background-color: #a9addd;
`;
