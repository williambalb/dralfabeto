import React from 'react';
import {StyleSheet} from 'react-native';
import {Container, Div, Title} from './styles';
import {SvgUri} from 'react-native-svg';
import api from './../../services/api';

const historys = (props) => {
  let backgroundColor = `#${props.background}`;
  return (
    <Container>
      <Div background={backgroundColor}>
        <SvgUri
          height={95}
          width={95}
          uri={`${api.defaults.baseURL}/svg/historys/icons/${props.svgName}.svg`}
          style={styles.svg}
        />
      </Div>
      <Title>{props.title}</Title>
    </Container>
  );
};

const styles = StyleSheet.create({
  svg: {
    position: 'absolute',
    bottom: 5,
  },
});

export default historys;
