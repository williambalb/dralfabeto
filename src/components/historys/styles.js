import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  width: 140px;
  height: 140px;
  margin: 10px;
  justify-content: flex-start;
  align-items: center;
`;

export const Div = styled.View`
  margin: 15px 0;
  width: 90px;
  height: 90px;
  border-radius: 10px;
  align-items: center;
  background-color: ${(props) => props.background};
`;

export const Title = styled.Text`
  color: #444;
  text-align: center;
  font-family: PoppinsBold;
  font-size: 12px;
`;
