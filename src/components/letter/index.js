import React from 'react';
import {useNavigation} from '@react-navigation/native';

import {Block, Lettering} from './styles';

const Letter = (props) => {
  const navigation = useNavigation();
  return (
    <Block>
      <Lettering
        vowel={props.vowel}
        onPress={() =>
          navigation.navigate('PresentingLetters', {
            letterNumber: props.id,
            letterName: props.name,
            letterInfo: props.infoSound,
          })
        }>
        {props.name}
      </Lettering>
    </Block>
  );
};

export default Letter;
