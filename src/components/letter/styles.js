import styled from 'styled-components/native';

export const Block = styled.TouchableOpacity`
  width: 80px;
  height: 80px;
  background-color: #1c4e71;
  justify-content: center;
  align-items: center;
  margin: 15px;
  border-radius: 50px;
  border: 8px solid #fff;
`;

export const Lettering = styled.Text`
  color: ${(props) => (props.vowel ? '#a9addd' : '#fff')};
  font-size: 45px;
  font-family: RifficBold;
`;
