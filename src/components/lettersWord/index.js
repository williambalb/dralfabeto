import React, {useEffect, useState} from 'react';
import {Block, Letter, Special, SpecialBlock} from './styles';
import SoundPlayer from 'react-native-sound-player';

const LettersWord = (props) => {
  const word = props.word;
  const [letters, setLetters] = useState();
  let letterListArr;

  useEffect(() => {
    let items = Array.from(word);
    setLetters(items);
  }, [word]);

  if (letters !== undefined) {
    letterListArr = letters.map((letterInfo, index) => {
      if (letterInfo === '-') {
        return <SpecialComponent key={index} letter={letterInfo} />;
      } else {
        return <LetterComponent key={index} letter={letterInfo} />;
      }
    });
  }

  if (letterListArr !== undefined) {
    return letterListArr;
  }

  return null;
};

const LetterComponent = (props) => {
  const bucketUrl =
    'https://dralfabetopolly.s3-sa-east-1.amazonaws.com/letters/';

  function playSound(letter) {
    let soundId = letter;
    try {
      SoundPlayer.loadUrl(bucketUrl + soundId + '.mp3');
      SoundPlayer.addEventListener('FinishedLoadingURL', ({success}) => {
        SoundPlayer.play();
      });
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <Block onPress={() => playSound(props.letter)}>
      <Letter>{props.letter}</Letter>
    </Block>
  );
};

const SpecialComponent = (props) => {
  return (
    <SpecialBlock>
      <Special>{props.letter}</Special>
    </SpecialBlock>
  );
};

export default LettersWord;
