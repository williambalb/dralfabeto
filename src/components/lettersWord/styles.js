import styled from 'styled-components/native';

export const Block = styled.TouchableOpacity`
  background-color: #fff;
  width: 24px;
  height: 24px;
  border-radius: 4px;
  border-style: solid;
  border-width: 1px;
  margin-right: 2px;
  border-color: #1c4e71;
  align-items: center;
  justify-content: center;
`;

export const SpecialBlock = styled.View`
  width: 15px;
  height: 24px;
  margin-right: 2px;
  border-color: #1c4e71;
  align-items: center;
  justify-content: center;
`;

export const Letter = styled.Text`
  color: #1c4e71;
  font-family: RifficBold;
  font-size: 16px;
  text-transform: uppercase;
`;

export const Special = styled.Text`
  color: #1c4e71;
  font-family: RifficBold;
  font-size: 20px;
`;
