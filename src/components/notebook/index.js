import React from 'react';
import {StyleSheet} from 'react-native';
import {Block, Capital, Handwriting} from './styles';
import Notebook from './../../assets/svg/vocabulary/notebook.svg';

const notebook = (props) => {
  return (
    <Block>
      <Notebook width={80} height={100} style={styles.note} />
      <Capital>{props.letter}</Capital>
      <Handwriting>{props.letter}</Handwriting>
    </Block>
  );
};

const styles = StyleSheet.create({
  note: {
    position: 'absolute',
  },
});

export default notebook;
