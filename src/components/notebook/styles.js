import styled from 'styled-components/native';

export const Block = styled.TouchableOpacity`
  width: 80px;
  height: 100px;
  margin: 15px;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-end;
  align-content: center;
  transform: rotate(-3deg);
`;

export const Capital = styled.Text`
  width: 60px;
  font-size: 30px;
  font-family: OpenSansLight;
  text-align: center;
  letter-spacing: 2px;
  margin-bottom: -5px;
  color: #fff;
`;

export const Handwriting = styled.Text`
  width: 60px;
  font-size: 30px;
  font-family: LumenFull;
  letter-spacing: 2px;
  text-align: center;
  color: #fff;
`;
