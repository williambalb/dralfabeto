import React from 'react';
import {useNavigation} from '@react-navigation/native';
import api from './../../services/api';

import {Bubble} from './styles';
import {SvgUri} from 'react-native-svg';

const Reading = (props) => {
  const navigation = useNavigation();
  return (
    <Bubble
      onPress={() => navigation.navigate('PresentingWords', props.wordSize)}>
      <SvgUri
        height={40}
        width={40}
        uri={`${api.defaults.baseURL}/svg/sections/icons/${props.svgName}.svg`}
      />
    </Bubble>
  );
};

export default Reading;
