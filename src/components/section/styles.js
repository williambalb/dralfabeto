import styled from 'styled-components/native';

export const Bubble = styled.TouchableOpacity`
  width: 80px;
  height: 80px;
  align-items: center;
  margin: 0 15px;
  justify-content: center;
  border: 8px solid #fff;
  border-radius: 50px;
  background-color: #1c4e71;
`;

export const Border = styled.View`
  width: 105px;
  height: 105px;
  border-radius: 50px;
  background-color: #bbb;
`;
