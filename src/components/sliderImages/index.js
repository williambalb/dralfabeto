import React from 'react';
import {Container, Div, Title} from './styles';
import {SvgUri} from 'react-native-svg';
import api from './../../services/api';

const sliderImages = (props) => {
  return (
    <Container>
      <Div>
        <SvgUri
          height={100}
          width={100}
          uri={`${api.defaults.baseURL}/svg/words/icons/${props.svg}.svg`}
        />
      </Div>
      <Title>{props.title}</Title>
    </Container>
  );
};

export default sliderImages;
