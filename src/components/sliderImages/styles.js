import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  padding-top: 10px;
  padding-bottom: 10px;
  justify-content: center;
  align-items: center;
`;

export const Div = styled.View`
  flex: 2;
  width: 200px;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  flex: 1;
  text-align: center;
  font-size: 40px;
  letter-spacing: 5px;
  font-family: PoppinsBold;
  text-transform: uppercase;
  color: #fff;
`;
