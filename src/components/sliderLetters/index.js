import React from 'react';
import {
  Container,
  Block,
  BlockUpper,
  BlockLower,
  HandUpper,
  HandLower,
} from './styles';

const sliderLetters = (props) => {
  if (props.type === 'Block') {
    return (
      <Container>
        <Block>
          {props.capitalize ? (
            <BlockUpper>{props.letter}</BlockUpper>
          ) : (
            <BlockLower>{props.letter}</BlockLower>
          )}
        </Block>
      </Container>
    );
  } else {
    return (
      <Container>
        <Block>
          {props.capitalize ? (
            <HandUpper>{props.letter}</HandUpper>
          ) : (
            <HandLower>{props.letter}</HandLower>
          )}
        </Block>
      </Container>
    );
  }
};

export default sliderLetters;
