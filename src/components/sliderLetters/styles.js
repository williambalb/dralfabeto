import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Block = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const BlockUpper = styled.Text`
  width: 100%;
  margin-top: -60px;
  text-align: center;
  color: #fff;
  font-size: 100px;
  font-family: OpenSans;
  text-transform: uppercase;
`;

export const BlockLower = styled.Text`
  width: 100%;
  margin-top: -60px;
  text-align: center;
  color: #fff;
  font-size: 100px;
  font-family: OpenSans;
  text-transform: lowercase;
`;

export const HandUpper = styled.Text`
  width: 100%;
  text-align: center;
  color: #fff;
  font-size: 100px;
  font-family: LumenFull;
  text-transform: uppercase;
`;

export const HandLower = styled.Text`
  width: 100%;
  margin-top: -10px;
  text-align: center;
  color: #fff;
  font-size: 100px;
  font-family: LumenFull;
  text-transform: lowercase;
`;
