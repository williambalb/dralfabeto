import React from 'react';
import {Container, Blackboard, Title} from './styles';
import Logo from '../../assets/svg/dr.svg';
import Background from './../background';
import Svgs from './../../assets/svg/splashscreen';

const splashscreen = () => {
  return (
    <Background>
      <Container>
        {/* <Svgs.A width={40} height={40} />
        <Svgs.B width={25} height={25} />
        <Svgs.M width={30} height={30} />
        <Svgs.O width={35} height={35} />
        <Svgs.P width={60} height={60} /> */}
        <Blackboard>
          <Logo width={140} height={140} />
        </Blackboard>
        <Title>Dr. Alfabeto</Title>
      </Container>
    </Background>
  );
};

export default splashscreen;
