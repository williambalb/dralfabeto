import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Blackboard = styled.View`
  padding: 20px 60px 0;
  background-color: #2e9169;
  border-width: 2px;
  border-bottom-width: 15px;
  border-color: #cabb84;
`;

export const Title = styled.Text`
  color: #fff;
  text-transform: uppercase;
  margin-top: 10px;
  font-size: 40px;
  font-family: RifficBold;
`;
