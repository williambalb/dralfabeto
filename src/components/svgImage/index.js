import React from 'react';
import {Block} from './styles';
import {SvgUri} from 'react-native-svg';
import SoundPlayer from 'react-native-sound-player';
import api from './../../services/api';

const svgImage = (props) => {
  const bucketUrl = 'https://dralfabetopolly.s3-sa-east-1.amazonaws.com/';

  function playSound(sound) {
    let soundId = sound;
    try {
      SoundPlayer.loadUrl(bucketUrl + soundId + '.mp3');
      SoundPlayer.addEventListener('FinishedLoadingURL', ({success}) => {
        SoundPlayer.play();
      });
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <Block onPress={() => playSound(props.sound)}>
      <SvgUri
        height={props.size}
        width={props.size}
        uri={`${api.defaults.baseURL}/svg/${props.source}/icons/${props.name}.svg`}
      />
    </Block>
  );
};

export default svgImage;
