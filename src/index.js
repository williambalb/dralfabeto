import React, {useState, useEffect} from 'react';
import {StatusBar} from 'react-native';
import SystemSetting from 'react-native-system-setting';

import SplashScreen from './components/splashscreen';
import Routes from './routes';
import Sound from './pages/enableSound';

const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [volume, setVolume] = useState(0);
  const [sound, setSound] = useState(false);

  const performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };

  useEffect(() => {
    let data;
    (async () => {
      data = await performTimeConsumingTask();
      if (data !== null) {
        return setIsLoading(false);
      }
    })();
  }, [isLoading]);

  SystemSetting.getVolume().then((currentVolume) => setVolume(currentVolume));

  const volumeListener = SystemSetting.addVolumeListener((currentVolume) => {
    setVolume(currentVolume.value);
  });

  useEffect(() => {
    if (volume > 0.3) {
      setSound(true);
    }
  }, [volume]);

  if (isLoading) {
    return (
      <>
        <StatusBar backgroundColor="#abd6ea" />
        <SplashScreen />
      </>
    );
  }

  if (!sound) {
    return (
      <>
        <Sound />
      </>
    );
  }

  SystemSetting.removeVolumeListener(volumeListener);

  return (
    <>
      <StatusBar backgroundColor="#abd6ea" />
      <Routes />
    </>
  );
};

export default App;
