import React, {useState, useEffect} from 'react';
import {Block} from './styles';
import SystemSetting from 'react-native-system-setting';

import Background from './../../components/background';
import SvgSound from './../../assets/svg/sound.svg';

const Home = () => {
  return (
    <Background>
      <Block>
        <SvgSound width={105} height={105} />
      </Block>
    </Background>
  );
};

export default Home;
