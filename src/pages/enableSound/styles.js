import styled from 'styled-components/native';

export const Block = styled.View`
  width: 150px;
  height: 150px;
  background-color: #1c4e71;
  border-radius: 25px;
  align-items: center;
  justify-content: center;
`;
