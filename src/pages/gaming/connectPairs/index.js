import React, {useState, useEffect} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {SvgUri} from 'react-native-svg';

import Background from './../../../components/background';
import ActionBar from './../../../components/actionbar';
import SoundButton from './../../../components/buttonSound';

import {
  GamingContainer,
  ImageContainer,
  WordContainer,
  ImageItem,
  WordItem,
  WordBlock,
} from './styles';

import api from './../../../services/api';

const ConnectPairs = ({route}) => {
  const {contentData} = route.params;

  const windowWidth = Dimensions.get('window').width;
  const [activityData, setActivityData] = useState([]);
  const [soundId, setSoundId] = useState();
  const [imgArr, setImgArr] = useState([]);
  const [wordArr, setWordArr] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadData() {
      api
        .get('/activity')
        .then((res) => {
          res.data.forEach((activity) => {
            if (activity._id === '60bab016f3cd16d29617df3f') {
              setSoundId(activity.instructionSound);
            }
          });
        })
        .then(() => {
          setActivityData(activityData);
        })
        .then(() => {
          contentData.forEach((item) => {
            setImgArr((arrImg) => [item.svg, ...arrImg]);
            setWordArr((arrWord) => [item.title, ...arrWord]);
          });
        })
        .catch((err) => console.log(err))
        .finally(() => {});
    }
    loadData();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (imgArr.length === 3) {
      setLoading(false);
    }
  }, [imgArr]);

  if (loading) {
    return <Background />;
  }

  return (
    <Background>
      <ActionBar backHome={true} />
      <GamingContainer style={{width: windowWidth}}>
        <ImageContainer>
          <ImageItems imgArr={imgArr} />
        </ImageContainer>
        <WordContainer>
          <WordItems wordArr={wordArr} />
        </WordContainer>
      </GamingContainer>
      <SoundButton soundType="instruction" soundId={soundId} />
    </Background>
  );
};

const ImageItems = (props) => {
  let arr = props.imgArr;
  let imgArr = shuffle(arr);
  const [pressed, setPressed] = useState([false, false, false]);

  let touchProps = {
    activeOpacity: 1,
    underlayColor: '#ffffff',
    style: pressed[0] ? styles.btnPressed : null,
    onPress: () => setSelected(0),
  };

  useEffect(() => {
    console.log('');
  }, [pressed]);

  function setSelected(index) {
    let newArr = pressed;
    for (let i = 0; i < pressed.length; i++) {
      i === index ? (newArr[i] = true) : (newArr[i] = false);
    }
    console.log(newArr);
    setPressed(newArr);
  }

  return (
    <>
      <ImageItem {...touchProps}>
        {imgArr[0] !== undefined ? (
          <SvgUri
            height={70}
            width={70}
            uri={`${api.defaults.baseURL}/svg/words/icons/${imgArr[0]}.svg`}
          />
        ) : null}
      </ImageItem>
      <ImageItem {...touchProps}>
        {imgArr[1] !== undefined ? (
          <SvgUri
            height={70}
            width={70}
            uri={`${api.defaults.baseURL}/svg/words/icons/${imgArr[1]}.svg`}
          />
        ) : null}
      </ImageItem>
      <ImageItem {...touchProps}>
        {imgArr[2] !== undefined ? (
          <SvgUri
            height={70}
            width={70}
            uri={`${api.defaults.baseURL}/svg/words/icons/${imgArr[2]}.svg`}
          />
        ) : null}
      </ImageItem>
    </>
  );
};

const WordItems = (props) => {
  let arr = props.wordArr;
  let wordArr = shuffle(arr);
  return (
    <>
      <WordItem>
        <WordBlock>{wordArr[0]}</WordBlock>
      </WordItem>
      <WordItem>
        <WordBlock>{wordArr[1]}</WordBlock>
      </WordItem>
      <WordItem>
        <WordBlock>{wordArr[2]}</WordBlock>
      </WordItem>
    </>
  );
};

function shuffle(array) {
  var currentIndex = array.length,
    randomIndex;
  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }
  return array;
}

const styles = StyleSheet.create({
  btnPressed: {
    backgroundColor: '#fff',
  },
});

export default ConnectPairs;
