import styled from 'styled-components/native';

export const GamingContainer = styled.View`
  flex: 4;
  padding: 10px;
  flex-direction: row;
`;

export const ImageContainer = styled.View`
  flex: 1;
  margin-right: 5px;
`;

export const ImageItem = styled.TouchableHighlight`
  flex: 1;
  margin: 5px 0;
  align-items: center;
  justify-content: center;
  border: 1px solid #fff;
  border-radius: 10px;
`;

export const WordContainer = styled.View`
  flex: 1;
  margin-left: 5px;
`;

export const WordItem = styled.TouchableHighlight`
  flex: 1;
  margin: 5px 0;
  align-items: center;
  justify-content: center;
  border: 1px solid #fff;
  border-radius: 10px;
`;

export const WordBlock = styled.Text`
  padding: 10px 20px;
  border-radius: 10px;
  text-align: center;
  background-color: #fff;
  font-family: RifficBold;
  font-size: 15px;
  color: #1c4e71;
  text-transform: uppercase;
  letter-spacing: 2px;
`;
