import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Container, Block, Button} from './styles';

import Background from './../../components/background';

import SvgBoy from './../../assets/svg/boy.svg';
import SvgGirl from './../../assets/svg/girl.svg';
import SvgSound from './../../assets/svg/sound.svg';

const gender = () => {
  return (
    <Background>
      <Container>
        <TouchableOpacity>
          <Block backColor={'#388697'}>
            <SvgBoy width={110} height={110} />
          </Block>
        </TouchableOpacity>
        <TouchableOpacity>
          <Block backColor={'#EBBAB9'}>
            <SvgGirl width={110} height={110} />
          </Block>
        </TouchableOpacity>
        <TouchableOpacity>
          <Button>
            <SvgSound width={50} height={50} />
          </Button>
        </TouchableOpacity>
      </Container>
    </Background>
  );
};

export default gender;
