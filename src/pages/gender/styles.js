import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: space-evenly;
`;

export const Block = styled.View`
  background-color: ${(props) => `${props.backColor}`};
  width: 180px;
  height: 180px;
  padding: 20px;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
`;

export const Button = styled.View`
  background-color: #cc2936;
  width: 180px;
  height: 70px;
  padding: 20px;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
`;
