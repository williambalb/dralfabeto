import React, {useState, useEffect} from 'react';
import {FlatList} from './styles';
import Background from './../../components/background';
import History from './../../components/historys';
import api from './../../services/api';

const Historys = () => {
  const [historysData, setHistorysData] = useState();

  useEffect(() => {
    async function loadData() {
      api
        .get('/historys')
        .then((res) => {
          setHistorysData(res.data);
        })
        .catch((err) => console.log(err))
        .finally(() => {});
    }
    loadData();
  }, []);

  return (
    <Background>
      <FlatList
        showsVerticalScrollIndicator={false}
        // eslint-disable-next-line react-native/no-inline-styles
        contentContainerStyle={{
          flexDirection: 'column',
          justifyContent: 'center',
        }}
        data={historysData}
        numColumns={2}
        keyExtractor={(item) => String(item._id)}
        renderItem={({item}) =>
          item._id !== undefined ? (
            <History
              title={item.title}
              svgName={item.image}
              background={item.background}
            />
          ) : null
        }
      />
    </Background>
  );
};

export default Historys;
