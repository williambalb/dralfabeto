import styled from 'styled-components/native';

export const FlatList = styled.FlatList``;

export const Scroll = styled.ScrollView`
  padding: 10px;
`;

export const Container = styled.View`
  flex: 1;
  margin-top: 10px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  flex-wrap: wrap;
`;
