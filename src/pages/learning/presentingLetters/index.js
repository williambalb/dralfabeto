import React, {useState, useEffect, useRef} from 'react';
import {Dimensions} from 'react-native';
import {
  ImagesContainer,
  LetterContainer,
  SoundContainer,
  SoundButton,
  LetterInfo,
} from './styles';
import SoundSvg from '../../../assets/svg/sound.svg';
import ActionBar from '../../../components/actionbar';
import SliderImages from '../../../components/sliderImages';
import SliderLetters from '../../../components/sliderLetters';
import Background from '../../../components/background';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import SoundPlayer from 'react-native-sound-player';
import api from './../../../services/api';

const windowWidth = Dimensions.get('window').width;

const PresentingLetters = ({route}) => {
  const {letterNumber, letterName, letterInfo} = route.params;

  const [isLoading, setIsLoading] = useState(true);
  const [wordsData, setWordsData] = useState();
  const [wordsImages, setWordsImages] = useState([]);
  const [activeIndexImage, setActiveIndexImage] = useState(0);
  const [activeIndexLetter, setActiveIndexLetter] = useState(0);
  const [played, setPlayed] = useState([]);
  const [completed, setCompleted] = useState(false);

  const ref = useRef(null);

  const bucketUrl = 'https://dralfabetopolly.s3-sa-east-1.amazonaws.com/';

  const performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve('result');
      }, 4000),
    );
  };

  useEffect(() => {
    async function loadData() {
      let words = [];
      api
        .get('/words')
        .then((res) => {
          res.data.forEach((word) => {
            if (word.initialId === letterNumber) {
              words.push(word);
            }
          });
        })
        .then(() => {
          setWordsData(words);
        })
        .catch((err) => console.log(err))
        .finally(() => {});
    }
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (wordsData !== undefined) {
      getImages(wordsData);
    }
  }, [wordsData]);

  useEffect(() => {
    let data;
    (async () => {
      data = await performTimeConsumingTask();
      if (data !== null) {
        return setIsLoading(false);
      }
    })();
  }, [isLoading]);

  useEffect(() => {
    if (played.includes(0) && played.includes(1) && played.includes(2)) {
      setCompleted(true);
    }
  }, [played]);

  function getImages(data) {
    data.forEach((item) => {
      let newElement = {
        svg: item.image,
        title: item.text,
        sound: item.sound,
      };
      setWordsImages((oldData) => [...oldData, newElement]);
    });
  }

  function playSound() {
    let soundId = wordsImages[activeIndexImage].sound;
    try {
      SoundPlayer.loadUrl(bucketUrl + soundId + '.mp3');
      SoundPlayer.addEventListener('FinishedLoadingURL', ({success}) => {
        SoundPlayer.play();
        if (played) {
          if (!played.includes(activeIndexImage)) {
            setPlayed([...played, activeIndexImage]);
          }
        }
      });
    } catch (e) {
      console.log(e);
    }
  }

  function playSoundInfo(soundId) {
    try {
      SoundPlayer.loadUrl(bucketUrl + 'presentation.' + soundId + '.mp3');
      SoundPlayer.addEventListener('FinishedLoadingURL', ({success}) => {
        SoundPlayer.play();
      });
    } catch (e) {
      console.log(e);
    }
  }

  let dataLetters = [
    {
      type: 'Block',
      capitalize: true,
    },
    {
      type: 'Block',
      capitalize: false,
    },
    {
      type: 'Hand',
      capitalize: true,
    },
    {
      type: 'Hand',
      capitalize: false,
    },
  ];

  function _renderItemImages({item, index}) {
    return <SliderImages svg={item.svg} title={item.title} />;
  }

  function _renderItemLetter({item, index}) {
    return (
      <SliderLetters
        letter={letterName}
        type={item.type}
        capitalize={item.capitalize}
      />
    );
  }

  if (isLoading) {
    playSoundInfo(letterInfo);
    return (
      <Background>
        <LetterInfo>{letterName}</LetterInfo>
      </Background>
    );
  }

  return (
    <Background>
      <ActionBar
        complete={completed}
        destiny="GamingConnectPairs"
        contentData={wordsImages}
      />
      <ImagesContainer>
        <Carousel
          ref={ref}
          windowSize={1}
          layout={'default'}
          data={wordsImages}
          renderItem={_renderItemImages}
          sliderWidth={windowWidth}
          itemWidth={windowWidth}
          onSnapToItem={(index) => setActiveIndexImage(index)}
        />
        <Pagination
          // eslint-disable-next-line react-native/no-inline-styles
          containerStyle={{paddingVertical: 15}}
          dotsLength={wordsImages.length}
          activeDotIndex={activeIndexImage}
          dotColor={'rgba(255, 255, 255, 0.92)'}
          inactiveDotColor={'#333'}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.7}
          animatedDuration={150}
        />
      </ImagesContainer>
      <LetterContainer style={{width: windowWidth}}>
        <Carousel
          ref={ref}
          windowSize={1}
          layout={'default'}
          data={dataLetters}
          renderItem={_renderItemLetter}
          sliderWidth={windowWidth}
          itemWidth={windowWidth}
          onSnapToItem={(index) => setActiveIndexLetter(index)}
        />
        <Pagination
          // eslint-disable-next-line react-native/no-inline-styles
          containerStyle={{paddingVertical: 15}}
          dotsLength={4}
          activeDotIndex={activeIndexLetter}
          dotColor={'rgba(255, 255, 255, 0.92)'}
          inactiveDotColor={'#333'}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.7}
          animatedDuration={150}
        />
      </LetterContainer>
      <SoundContainer style={{width: windowWidth}}>
        <SoundButton onPress={() => playSound()}>
          <SoundSvg width={50} height={50} />
        </SoundButton>
      </SoundContainer>
    </Background>
  );
};

export default PresentingLetters;
