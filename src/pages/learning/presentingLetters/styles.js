import styled from 'styled-components/native';

export const ImagesContainer = styled.View`
  flex: 2.5;
`;

export const LetterContainer = styled.View`
  flex: 2;
`;

export const SoundContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 10px;
`;

export const SoundButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  width: 100%;
  height: 60px;
  background-color: #a9addd;
`;

export const LetterInfo = styled.Text`
  font-size: 200px;
  text-align: center;
  color: #fff;
  font-family: OpenSans;
`;
