import React, {useEffect, useState} from 'react';
import {FlatList, InfoContainer, ImageContainer, TextContainer} from './styles';
import ActionBar from '../../../components/actionbar';
import SvgImage from './../../../components/svgImage';
import LettersWord from './../../../components/lettersWord';

import Background from '../../../components/background';

import api from './../../../services/api';

const PresentingWords = ({route}) => {
  const wordSize = route.params;
  const [wordsFiltered, setWordsFiltered] = useState();

  useEffect(() => {
    async function loadData() {
      let words = [];
      api
        .get('/words')
        .then((res) => {
          res.data.forEach((word) => {
            if (word.lenght === wordSize) {
              words.push(word);
            }
          });
        })
        .then(() => {
          setWordsFiltered(words);
        })
        .catch((err) => console.log(err))
        .finally(() => {});
    }
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function _renderItems({item}) {
    if (item.image !== undefined) {
      return (
        <InfoContainer>
          <ImageContainer>
            <SvgImage
              size={50}
              source="words"
              name={item.image}
              sound={item.sound}
            />
          </ImageContainer>
          <TextContainer>
            <LettersWord word={item.text} />
          </TextContainer>
        </InfoContainer>
      );
    } else {
      return null;
    }
  }

  return (
    <Background>
      <ActionBar />
      <FlatList
        showsVerticalScrollIndicator={false}
        // eslint-disable-next-line react-native/no-inline-styles
        contentContainerStyle={{
          flexDirection: 'column',
        }}
        data={wordsFiltered}
        numColumns={1}
        keyExtractor={(item) => String(item._id)}
        renderItem={_renderItems}
      />
    </Background>
  );
};

export default PresentingWords;
