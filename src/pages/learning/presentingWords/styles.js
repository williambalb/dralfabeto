import styled from 'styled-components/native';

export const FlatList = styled.FlatList``;

export const InfoContainer = styled.View`
  width: 350px;
  flex-direction: row;
  height: 80px;
  margin: 10px;
`;

export const ImageContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const TextContainer = styled.View`
  flex: 3.5;
  flex-direction: row;
  align-items: center;
`;

export const SoundButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  width: 100%;
  height: 60px;
`;
