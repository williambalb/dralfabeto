import React, {useState, useEffect} from 'react';
import {FlatList} from './styles';
import Letter from './../../components/letter';
import Background from './../../components/background';
import api from './../../services/api';

const Letters = () => {
  const [alfabetData, setAlfabetData] = useState();

  useEffect(() => {
    async function loadData() {
      api
        .get('/alfabet')
        .then((res) => {
          setAlfabetData(res.data);
        })
        .catch((err) => console.log(err))
        .finally(() => {});
    }
    loadData();
  }, []);

  return (
    <Background>
      <FlatList
        showsVerticalScrollIndicator={false}
        // eslint-disable-next-line react-native/no-inline-styles
        contentContainerStyle={{
          flexDirection: 'column',
          justifyContent: 'center',
        }}
        data={alfabetData}
        numColumns={3}
        keyExtractor={(item) => String(item.number)}
        renderItem={({item}) =>
          item.vowel === true ? (
            <Letter
              name={item.letter.toUpperCase()}
              vowel={item.vowel}
              id={item.number}
              infoSound={item.letterInfo}
            />
          ) : (
            <Letter name={item.letter.toUpperCase()} id={item.number} />
          )
        }
      />
    </Background>
  );
};

export default Letters;
