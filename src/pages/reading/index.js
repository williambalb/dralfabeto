import React, {useState, useEffect} from 'react';
import {Block} from './styles';
import {FlatList} from './styles';
import Background from './../../components/background';
import Section from './../../components/section';
import api from './../../services/api';

const Reading = () => {
  const [readingData, setReadingData] = useState();

  useEffect(() => {
    async function loadData() {
      api
        .get('/reading')
        .then((res) => {
          let groups = [];
          res.data.map((group) => {
            if (groups[group.line] !== undefined) {
              groups[group.line].lineData.push(group);
            } else {
              groups[group.line] = {
                id: group.line,
                lineData: [],
              };
              groups[group.line].lineData.push(group);
            }
          });
          setReadingData(groups);
        })
        .catch((err) => console.log(err))
        .finally(() => {});
    }
    loadData();
  }, []);

  function getGroups(lineData) {
    switch (lineData.length) {
      case 1:
        return (
          <Block>
            <Section
              svgName={lineData[0].image}
              wordSize={lineData[0].wordSize}
            />
          </Block>
        );
      case 2:
        return (
          <Block>
            <Section
              svgName={lineData[0].image}
              wordSize={lineData[0].wordSize}
            />
            <Section
              svgName={lineData[1].image}
              wordSize={lineData[1].wordSize}
            />
          </Block>
        );
      default:
        return (
          <Block>
            <Section
              svgName={lineData[0].image}
              wordSize={lineData[0].wordSize}
            />
            <Section
              svgName={lineData[1].image}
              wordSize={lineData[1].wordSize}
            />
            <Section
              svgName={lineData[2].image}
              wordSize={lineData[2].wordSize}
            />
          </Block>
        );
    }
  }

  return (
    <Background>
      <FlatList
        showsVerticalScrollIndicator={false}
        // eslint-disable-next-line react-native/no-inline-styles
        contentContainerStyle={{
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        data={readingData}
        keyExtractor={(item) => String(item.id)}
        renderItem={({item}) =>
          item.id !== undefined ? getGroups(item.lineData) : null
        }
      />
    </Background>
  );
};

export default Reading;
