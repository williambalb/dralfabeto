import styled from 'styled-components/native';

export const FlatList = styled.FlatList``;

export const Block = styled.View`
  width: 100%;
  height: 110px;
  margin-bottom: 20px;
  flex-direction: row;
  align-items: center;
`;
