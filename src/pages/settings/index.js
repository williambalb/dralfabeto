import React from 'react';
import {
  CardBlock,
  TitleBlock,
  Title,
  ChallengeBlock,
  ChallengeText,
  OptionsBlock,
  OptionButton,
  OptionText,
} from './styles';
import Background from './../../components/background';

const settings = () => {
  let initialArr = [2, 6, 5, 8, 0, 4];

  return (
    <Background>
      <CardBlock>
        <TitleBlock>
          <Title>Para os Pais</Title>
        </TitleBlock>
        <ChallengeBlock>
          <ChallengeText>12 - 7</ChallengeText>
        </ChallengeBlock>
        <OptionsBlock>
          {initialArr.map((prop, key) => {
            return (
              <OptionButton>
                <OptionText>{prop}</OptionText>
              </OptionButton>
            );
          })}
        </OptionsBlock>
      </CardBlock>
    </Background>
  );
};

export default settings;
