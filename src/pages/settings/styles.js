import styled from 'styled-components/native';

export const CardBlock = styled.View`
  width: 300px;
  height: 350px;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
  background-color: #1c4e71;
`;

export const TitleBlock = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  color: #fff;
  text-align: center;
  font-family: MontserratBold;
  font-size: 30px;
`;

export const ChallengeBlock = styled.View`
  flex: 1;
`;

export const ChallengeText = styled.Text`
  color: #e08bb2;
  font-family: MontserratBold;
  font-size: 45px;
`;

export const OptionsBlock = styled.View`
  flex: 3;
  width: 100%;
  justify-content: space-around;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const OptionButton = styled.TouchableOpacity`
  margin: 20px;
  width: 50px;
  height: 50px;
  border-radius: 25px;
  background-color: #e08bb2;
`;

export const OptionText = styled.Text`
  color: #fff;
  text-align: center;
  font-family: MontserratBold;
  font-size: 35px;
`;
