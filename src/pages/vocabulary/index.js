import React, {useState, useEffect} from 'react';
import {FlatList} from './styles';
import Notebook from './../../components/notebook';
import Background from './../../components/background';
import api from './../../services/api';

const Vocabulary = () => {
  const [vocabularyData, setVocabularyData] = useState();

  useEffect(() => {
    async function loadData() {
      api
        .get('/alfabet')
        .then((res) => {
          setVocabularyData(res.data);
        })
        .catch((err) => console.log(err))
        .finally(() => {});
    }
    loadData();
  }, []);

  return (
    <Background>
      <FlatList
        showsVerticalScrollIndicator={false}
        // eslint-disable-next-line react-native/no-inline-styles
        contentContainerStyle={{
          flexDirection: 'column',
          justifyContent: 'center',
        }}
        data={vocabularyData}
        numColumns={3}
        keyExtractor={(item) => String(item.number)}
        renderItem={({item}) => (
          <Notebook letter={`${item.letter.toUpperCase()}${item.letter}`} />
        )}
      />
    </Background>
  );
};

export default Vocabulary;
