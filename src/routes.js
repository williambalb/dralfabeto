import React from 'react';
import {StyleSheet} from 'react-native';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import LettersScreen from './pages/letters';
import ReadingScreen from './pages/reading';
import HistorysScreen from './pages/historys';
import VocabularyScreen from './pages/vocabulary';
import SettingsScreen from './pages/settings';

import PresentingLetters from './pages/learning/presentingLetters';
import PresentingWords from './pages/learning/presentingWords';

import ConnectPairs from './pages/gaming/connectPairs';

import Svgs from './assets/svg/tab_bar';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

function Home() {
  return (
    <Tab.Navigator
      initialRouteName="Letter"
      labeled={false}
      barStyle={styles.barStyle}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          if (route.name === 'Letters') {
            if (focused) {
              return <Svgs.Letters width={35} height={35} />;
            }
            return <Svgs.LettersLinear width={35} height={35} />;
          }
          if (route.name === 'Reading') {
            if (focused) {
              return <Svgs.Reading width={35} height={35} />;
            }
            return <Svgs.ReadingLinear width={35} height={35} />;
          }
          if (route.name === 'Historys') {
            if (focused) {
              return <Svgs.Historys width={30} height={30} />;
            }
            return <Svgs.HistorysLinear width={30} height={30} />;
          }
          if (route.name === 'Vocabulary') {
            if (focused) {
              return <Svgs.Vocabulary width={30} height={30} />;
            }
            return <Svgs.VocabularyLinear width={30} height={30} />;
          }
          if (route.name === 'Settings') {
            if (focused) {
              return <Svgs.Settings width={30} height={30} />;
            }
            return <Svgs.SettingsLinear width={30} height={30} />;
          }
        },
      })}>
      <Tab.Screen name="Letters" component={LettersScreen} />
      <Tab.Screen name="Reading" component={ReadingScreen} />
      <Tab.Screen name="Historys" component={HistorysScreen} />
      <Tab.Screen name="Vocabulary" component={VocabularyScreen} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
    </Tab.Navigator>
  );
}

function Routes() {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen
            name="PresentingLetters"
            component={PresentingLetters}
          />
          <Stack.Screen name="PresentingWords" component={PresentingWords} />
          <Stack.Screen name="GamingConnectPairs" component={ConnectPairs} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  barStyle: {
    height: 70,
    justifyContent: 'center',
    backgroundColor: '#abd6ea',
    borderTopColor: '#8bb4c7',
    borderTopWidth: 1.5,
  },
});

export default Routes;
