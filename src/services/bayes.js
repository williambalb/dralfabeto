const jsbayes = require('jsbayes');

let graph = jsbayes.newGraph();

let concept = graph.addNode('concept', ['true', 'false']);
let activity = graph.addNode('activity', ['true', 'false']);
let knowledge = graph.addNode('knowledge', ['true', 'false']);

activity.addParent(concept);
knowledge.addParent(concept).addParent(activity);

concept.cpt = [0.65, 0.35];
activity.cpt = [
  [0.85, 0.15],
  [0.25, 0.75],
];
knowledge.cpt = [
  [
    [0.95, 0.05],
    [0.12, 0.88],
  ],
  [
    [0.4, 0.6],
    [0, 1],
  ],
];
