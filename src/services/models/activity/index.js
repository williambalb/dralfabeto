const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const activitySchema = new Schema({
  description: String,
  concept_id: Number,
  difficulty: String,
  probability: Number,
  instructionSound: String,
});

module.exports = mongoose.model('activity', activitySchema, 'activity');
