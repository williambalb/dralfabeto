const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const letterSchema = new Schema({
  letter: String,
  number: Number,
  vowel: Boolean,
});

module.exports = mongoose.model('alfabet', letterSchema, 'alfabet');
