const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const historySchema = new Schema({
  title: String,
  image: String,
  background: String,
});

module.exports = mongoose.model('historys', historySchema, 'historys');
