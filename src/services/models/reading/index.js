const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const readingSchema = new Schema({
  level: Number,
  wordSize: Number,
  imageURI: String,
});

module.exports = mongoose.model('reading', readingSchema, 'reading');
