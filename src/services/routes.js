const router = require('express').Router();
let Alfabet = require('./models/alfabet');
let Activity = require('./models/activity');
let Reading = require('./models/reading');
let Historys = require('./models/historys');
let Words = require('./models/words');

router.route('/').get((req, res) => {
  res.send('Root');
});

router.route('/alfabet').get((req, res) => {
  Alfabet.find({}, function (err, letters) {
    if (err) {
      console.log(err);
    } else {
      res.json(letters);
    }
  });
});

router.route('/activity').get((req, res) => {
  Activity.find({}, function (err, activitys) {
    if (err) {
      console.log(err);
    } else {
      res.json(activitys);
    }
  });
});

router.route('/reading').get((req, res) => {
  Reading.find({}, function (err, groups) {
    if (err) {
      console.log(err);
    } else {
      res.json(groups);
    }
  });
});

router.route('/historys').get((req, res) => {
  Historys.find({}, function (err, historys) {
    if (err) {
      console.log(err);
    } else {
      res.json(historys);
    }
  });
});

router.route('/words').get((req, res) => {
  Words.find({}, function (err, words) {
    if (err) {
      console.log(err);
    } else {
      res.json(words);
    }
  });
});

console.log(router);

module.exports = router;
