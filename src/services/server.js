const express = require('express');
const app = express();
const mongoose = require('mongoose');
const router = require('./routes.js');
const cors = require('cors');
const _assets = '/home/ubuntu/app/dralfabeto/src/assets/svg';

app.use(cors());

mongoose.connect(
  'mongodb+srv://admin:DhuYF04VapEAVvyX@dr-alfabeto.ceslc.mongodb.net/dr-alfabeto?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  function (err) {
    if (err) {
      throw err;
    }
  },
);

const connection = mongoose.connection;

connection.once('open', () => {
  console.log('Ok');
});
app.use('/', router);
app.use('/activity', router);
app.use('/alfabet', router);
app.use('/reading', router);
app.use('/historys', router);
app.use('/words', router);
app.use('/svg', express.static(_assets));

app.listen(3000, () => {
  console.log('Rodando na porta 3000');
});
